import json
import logging
import src.api.representativecollector as RepresentativeCollector
import src.api.rolecollector as RoleCollector

# These are the values that can be entered into the config.json
# log > level field
logLevels = {
    "NOTSET":   logging.NOTSET,
    "DEBUG":    logging.DEBUG,
    "INFO":     logging.INFO,
    "WARN":     logging.WARNING,
    "ERROR":    logging.ERROR,
    "CRITICAL": logging.CRITICAL
}

# Opens the config file and stores it to config.
with open('src/config.json') as configFile:
    config = json.load(configFile)

# Configure logging as it is entered in the config file.
logging.basicConfig(filename=config["log"]["file"],
                    format=config["log"]["format"],
                    level=logLevels[config["log"]["level"]])

# RepresntativeCollector will start collecting upon initialization.
# RepresentativeCollector.RepresentativeCollector(config)

# RoleCollector will start collecting upon initialization.
RoleCollector.RoleCollector(config)
