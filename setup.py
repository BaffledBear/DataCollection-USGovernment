try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup
    
config = {
    'description': 'Repreality 2.0',
    'author': 'Peter Rauhut',
    'url': 'tbd',
     'download_url': 'tbd',
     'author_email': 'baffling.bear@gmail.com',
     'version': '0.1',
     'install_requires': ['nose'],
     'packages': ['repreal'],
     'scripts': [],
     'name': 'Represent Reality'
}   
 
setup(**config)
