/*
 * Author: Peter Rauhut
 * Title: createTables.sql
 * Project: DataCollection-USGovernment
 * Purpose: Build the Database and make it ready for interaction.
 */  

-- Reference Table for the States and Territories
-- that have representation in the US government
CREATE TABLE States (
    id              smallint primary key,
    name            varchar(25),
    short_name      char(2)
);

INSERT INTO States ( id, name, short_name )
    VALUES ( -1, Null, Null );

INSERT INTO States ( id, name, short_name ) 
    VALUES ( 1, 'Alabama', 'AL' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 2, 'Alaska', 'AK' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 3, 'American Samoa', 'AS' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 4, 'Arizona', 'AZ' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 5, 'Arkansas', 'AR' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 6, 'California', 'CA' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 7, 'Colorado', 'CO' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 8, 'Connecticut', 'CT' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 9, 'Delaware', 'DE' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 10, 'District of Columbia', 'DC' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 11, 'Florida', 'FL' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 12, 'Georgia', 'GA' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 13, 'Guam', 'GU' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 14, 'Hawaii', 'HI' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 15, 'Idaho', 'ID' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 16, 'Illinois', 'IL' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 17, 'Indiana', 'IN' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 18, 'Iowa', 'IA' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 19, 'Kansas', 'KS' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 20, 'Kentucky', 'KY' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 21, 'Louisiana', 'LA' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 22, 'Maine', 'ME' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 23, 'Maryland', 'MD' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 24, 'Massachusetts', 'MA' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 25, 'Michigan', 'MI' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 26, 'Minnesota', 'MN' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 28, 'Mississippi', 'MS' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 29, 'Missouri', 'MO' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 30, 'Montana', 'MT' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 31, 'Nebraska', 'NE' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 32, 'Nevada', 'NV' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 33, 'New Hampshire', 'NH' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 34, 'New Jersey', 'NJ' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 35, 'New Mexico', 'NM' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 36, 'New York', 'NY' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 37, 'North Carolina', 'NC' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 38, 'North Dakota', 'ND' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 39, 'Northern Mariana Islands', 'MP' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 40, 'Ohio', 'OH' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 41, 'Oklahoma', 'OK' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 42, 'Oregon', 'OR' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 43, 'Pennsylvania', 'PA' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 44, 'Puerto Rico', 'PR' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 45, 'Rhode Island', 'RI' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 46, 'South Carolina', 'SC' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 47, 'South Dakota', 'SD' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 48, 'Tennessee', 'TN' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 49, 'Texas', 'TX' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 50, 'U.S. Virgin Islands', 'VI' );

INSERT INTO States ( id, name, short_name ) 
    VALUES ( 51, 'Utah', 'UT' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 52, 'Vermont', 'VT' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 53, 'Virginia', 'VA' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 54, 'Washington', 'WA' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 55, 'West Virginia', 'WV' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 56, 'Wisconsin', 'WI' );
    
INSERT INTO States ( id, name, short_name ) 
    VALUES ( 57, 'Wyoming', 'WY' );
    
-- Reference table for the class of Senators
-- Classes are broken in 3 and define when a 
-- Senator is up for re-election.
CREATE TABLE Senate_Class (
    id              smallint primary key,
    label           char(7)
);

INSERT INTO Senate_Class ( id, label )
    VALUES ( -1, Null );

INSERT INTO Senate_Class ( id, label )
    VALUES ( 1, 'Class 1');

INSERT INTO Senate_Class ( id, label )
    VALUES ( 2, 'Class 2');

INSERT INTO Senate_Class ( id, label )
    VALUES ( 3, 'Class 3');

-- Reference Table for the rank of Senators
-- Rank is either Senior or Junior
CREATE TABLE Senate_Rank (
    id              smallint primary key,
    label           char(6)
);

INSERT INTO Senate_Rank ( id, label )
    VALUES ( -1, Null );

INSERT INTO Senate_Rank ( id, label )
    VALUES ( 1, 'Senior' );

INSERT INTO Senate_Rank ( id, label )
    VALUES ( 2, 'Junior' );

-- Reference table for the chambers of Congress
-- Senate and House of Representatives
CREATE TABLE Chamber ( 
    id              smallint primary key,
    label           varchar(9)
);

INSERT INTO Chamber ( id, label )
    VALUES ( 1, 'Senate' );

INSERT INTO Chamber ( id, label )
    VALUES ( 2, 'House' );

INSERT INTO Chamber ( id, label )
    VALUES ( 3, 'Executive');

-- Reference table for a persons Title
-- Delegates are from outlying territories
CREATE TABLE Title (
    id              smallint primary key,
    label           varchar(14)
);

INSERT INTO Title ( id, label )
    VALUES ( 1, 'President' );

INSERT INTO Title ( id, label )
    VALUES ( 2, 'Vice President');

INSERT INTO Title ( id, label )
    VALUES ( 3, 'Senator' );

INSERT INTO Title ( id, label )
    VALUES ( 4, 'Representative' );

INSERT INTO Title ( id, label )
    VALUES ( 5, 'Delegate' );


-- Table to hold the referencial ids for the leadership
-- titles that are given to the leaders in Congress
CREATE TABLE Leadership_Title (
    id              smallint primary key,
    label           varchar(15)
);

INSERT INTO Leadership_Title ( id, label )
    VALUES ( -1, Null );

INSERT INTO Leadership_Title ( id, label )
    VALUES ( 1, 'Speaker' );

INSERT INTO Leadership_Title ( id, label )
    VALUES ( 2, 'Majority Leader' );

INSERT INTO Leadership_Title ( id, label )
    VALUES ( 3, 'Minority Leader' );

INSERT INTO Leadership_Title ( id, label )
    VALUES ( 4, 'Majority Whip' );

INSERT INTO Leadership_Title ( id, label )
    VALUES ( 5, 'Minority Whip' );

-- Table to hold people
CREATE TABLE Representative (
    id              integer primary key,
    firstname       varchar(40),
    middlename      varchar(40),
    lastname        varchar(40),
    name_suffix     varchar(3),
    nickname        varchar(100),
    gender          char(1),
    birthday        date,
    chamber_id      smallint references Chamber(id),
    state_id        smallint references States(id),
    rank_id         smallint references Senate_Rank(id),
    party           char(1),
    senate_class_id smallint references Senate_Class(id),
    title_id        smallint references Title(id),
    leadership_id   smallint references Leadership_Title(id),
    term_start      date,
    term_end        date,
    office_add      varchar(100),
    phone_no        char(13),
    fax_no          char(13),
    oc_email        varchar(80),
    website         varchar(100),
    facebook_id     varchar(25),
    twitter_id      varchar(100),
    youtube_id      varchar(100),
    govtrack_id     varchar(100),
    bioguide_id     varchar(15),
    os_id           varchar(15),
    votesmart_id    integer,
    cspan_id        integer,
    icpsr_id        integer,
    thomas_id       integer,
    govtrack_link   varchar(100)
);

-- Table for the Federal Election Commission IDs
-- This was broken out because there are often
-- more than one ID for a rep
CREATE TABLE FEC_ID (
    id              integer primary key,
    rep_id          integer references Representative(id),
    fid             varchar(100)
);

-- Table for the Roles of elected officials in 
-- the US Government.
CREATE TABLE Role (
    id              integer primary key,
    description     text,
    district        smallint,
    term_start      date,
    term_end        date,
    govtrack_id     integer,
    title_id        smallint references Title(id),
    rep_id          integer references Representative(id)
);


