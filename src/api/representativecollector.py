import requests
import logging
import src.models.representative as Representative


class RepresentativeCollector:

    """
    This class will control the collection of data related to representatives
    of any kind.
    """
    # Config is assumed to be JSON pulled from config.json in the base
    # directory

    def __init__(self, config):
        """
        Initialize the necessary variables using values from config.
        """
        logging.info("Initializing RepresentativeCollector")
        self.config = config
        logging.debug(self.config)
        govtrack = config["govtrack"]
        sunlight = config["sunlight_congress"]
        govtrack_api = govtrack["apiBaseLink"]
        sunlight_api = sunlight["apiBaseLink"]
        self.sunlight_key = config["api_keys"]["Sunlight Foundation"]
        self.govtrack_all = str(govtrack_api +
                                govtrack["apiSubLinks"]
                                ["All Representatives"]
                                ["link"] +
                                "?current=true&limit=%d" %
                                int(govtrack["limit"]))
        logging.info("Govtrack All: %s" % self.govtrack_all)
        self.govtrack_rep_link = str(govtrack_api + govtrack["apiSubLinks"]
                                     ["Representative"]
                                     ["link"])
        logging.info("GovTrack Rep Link: %s" % self.govtrack_rep_link)
        self.sunlight_rep_link = str(sunlight_api +
                                     sunlight["apiSubLinks"]
                                     ["Representative"]
                                     ["link"] %
                                     "?bioguide_id=%s&apikey=%s")
        logging.info("Sunlight Foundation Rep Link: %s" %
                     self.sunlight_rep_link)
        self.collect()

    def collect(self):
        """
        Collect each rep and write it to the database.
        """
        representative_list_request = requests.get(self.govtrack_all)
        logging.info("Response: %s" % representative_list_request)

        representative_list = representative_list_request.json()["objects"]

        for representative in representative_list:
            bioguide_id = representative["person"]["bioguideid"]
            govtrack_rep = self.make_govtrack_rep(representative)
            logging.debug(govtrack_rep)
            logging.info("Working on: %s %s, %s" % (
                govtrack_rep["firstname"],
                govtrack_rep["lastname"],
                govtrack_rep["title"]))
            try:
                sunlight_rep_request = requests.get(
                    self.sunlight_rep_link % (
                        bioguide_id, self.sunlight_key
                    ))
                logging.info("Response: %s" % sunlight_rep_request)
                sunlight_rep = sunlight_rep_request.json()["results"][0]
                logging.debug(sunlight_rep)
            except IndexError:
                logging.info("No results for %s %s, %s" % (
                    govtrack_rep["firstname"],
                    govtrack_rep["lastname"],
                    govtrack_rep["title"]))
                sunlight_rep = {}

            rep = Representative.Representative(
                govtrack_rep,
                sunlight_rep,
                self.config)
            rep.write_to_database()
            # break  # uncomment during dev to perform single iteration

    def make_govtrack_rep(self, gt_rep):
        """
        Use gt_rep to build a dictionary defining a single representative and
        return it.
        """
        rep = {
            "firstname":        gt_rep["person"]["firstname"],
            "middlename":       gt_rep["person"]["middlename"],
            "lastname":         gt_rep["person"]["lastname"],
            "name_suffix":      gt_rep["person"]["namemod"],
            "nickname":         gt_rep["person"]["nickname"],
            "gender":           gt_rep["person"]["gender"][0],
            "birthday":         gt_rep["person"]["birthday"],
            "chamber":          gt_rep["title"][0:3],
            "state":            gt_rep["state"],
            "rank":             gt_rep["senator_rank"],
            "party":            gt_rep["party"][0],
            "senate_class":     gt_rep["senator_class"][-1] if
            gt_rep["senator_class"] else "",
            "title":            gt_rep["title"][0:3],
            "leadership_title": gt_rep["leadership_title"],
            "term_start":       gt_rep["startdate"],
            "term_end":         gt_rep["enddate"],
            "phone_no":         gt_rep["phone"],
            "website":          gt_rep["website"],
            "twitter_id":       gt_rep["person"]["twitterid"],
            "youtube_id":       gt_rep["person"]["youtubeid"],
            "govtrack_id":      gt_rep["person"]["id"],
            "bioguide_id":      gt_rep["person"]["bioguideid"],
            "os_id":            gt_rep["person"]["osid"],
            "votesmart_id":     gt_rep["person"]["pvsid"],
            "cspan_id":         gt_rep["person"]["cspanid"],
            "govtrack_link":    gt_rep["person"]["link"],
        }
        return rep
