import psycopg2
import logging

"""
A class representing the roles of elected officials. This is used by the
RoleCollector to store and manipulate data on the role that is currently
being processed.
"""


class Role:

    def __init__(self, role_dict, config):
        """
        During initialization, data is gathered from provided sources and
        stored in variables waiting to be processed.
        """
        self.database = psycopg2.connect(
            "dbname=%s user=%s password=%s host=%s" % (
                config["database"]["name"],
                config["database"]["username"],
                config["database"]["password"],
                config["database"]["host"]
            )
        )
        logging.debug(self.database)
        self.description = role_dict["description"]
        self.district = role_dict["district"]
        self.term_start = role_dict["term_start"]
        self.term_end = role_dict["term_end"]
        self.govtrack_id = role_dict["govtrack_id"]
        self.title_id = self.__get_title_id(role_dict["title"])
        self.rep_id = self.__get_rep_id(role_dict["rep_gt_id"])
        self.__log_role()

    def __get_title_id(self, title):
        """
        Read the id of the title for this role from the DB and return it.
        """
        cur = self.database.cursor()

        cur.execute("SELECT id FROM Title                                     \
            WHERE label = '%s'" % title)
        try:
            title_id = cur.fetchone()[0]
        except:
            title_id = 5
        cur.close()
        return title_id

    def __get_rep_id(self, gt_id):
        """
        Read the id for the representative for this role from the DB and return
        it.
        """
        cur = self.database.cursor()

        cur.execute("SELECT id FROM Representative                            \
            WHERE govtrack_id = '%s'" % gt_id)
        rep_id = cur.fetchone()[0]
        cur.close()
        return rep_id

    def get_role(self):
        """
        Return a dictionary of the role.
        """
        return {
            "description": self.description,
            "district": self.district,
            "term_start": self.term_start,
            "term_end": self.term_end,
            "govtrack_id": self.govtrack_id,
            "title_id": self.title_id,
            "rep_id": self.rep_id
        }

    def write_to_database(self):
        """
        Write the role to the role table in the database.
        This will UPDATE a role that exists and INSERT one that does not.
        """
        cur = self.database.cursor()

        cur.execute("SELECT id FROM Role                                      \
            WHERE govtrack_id = '%s';" % self.govtrack_id)
        if cur.fetchone():
            cur.execute("UPDATE Role SET                                      \
                description = %s,                                             \
                district = %s,                                                \
                term_start = %s,                                              \
                term_end = %s,                                                \
                govtrack_id = %s,                                             \
                title_id = %s,                                                \
                rep_id = %s                                                   \
                WHERE govtrack_id = '%s';", (
                self.description,
                self.district,
                self.term_start,
                self.term_end,
                self.govtrack_id,
                self.title_id,
                self.rep_id,
                self.govtrack_id))
        else:
            cur.execute("SELECT max(id) FROM Role;")
            self.role_id = cur.fetchone()[0]
            if self.role_id is None:
                self.role_id = 1
            else:
                self.role_id += 1
            cur.execute("INSERT INTO Role (                                   \
                id,                                                           \
                description,                                                  \
                district,                                                     \
                term_start,                                                   \
                term_end,                                                     \
                govtrack_id,                                                  \
                title_id,                                                     \
                rep_id) VALUES (                                              \
                %s,                                                           \
                %s,                                                           \
                %s,                                                           \
                %s,                                                           \
                %s,                                                           \
                %s,                                                           \
                %s,                                                           \
                %s                                                            \
                );",
                        (
                            self.role_id,
                            self.description,
                            self.district,
                            self.term_start,
                            self.term_end,
                            self.govtrack_id,
                            self.title_id,
                            self.rep_id
                        ))
        self.database.commit()
        cur.close()

    def __log_role(self):
        """
        Print all data that is stored about the role into the log under DEBUG.
        """
        logging.debug("Current Role: \n\
            description = %s\n\
            district = %s\n\
            term_start = %s\n\
            term_end = %s\n\
            govtrack_id = %s\n\
            title_id = %s\n\
            rep_id = %s\n" % (
            self.description,
            self.district,
            self.term_start,
            self.term_end,
            self.govtrack_id,
            self.title_id,
            self.rep_id))
