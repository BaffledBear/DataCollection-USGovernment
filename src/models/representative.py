import psycopg2
import logging

"""
A class representing an elected official. This is used by the
RepresentativeCollector to store and manipulate data on the representative that
is currently being processed.
"""


class Representative:

    def __init__(self, gt_rep, sl_rep, config):
        """
        During initialization, data is gathered from the provided sources and
        stored in variables waiting to be processed.
        """
        self.database = psycopg2.connect(
            "dbname=%s user=%s password=%s host=%s" % (
                config["database"]["name"],
                config["database"]["username"],
                config["database"]["password"],
                config["database"]["host"]
            )
        )
        logging.debug(self.database)
        self.firstname = self.__get_complete_value(
            gt_rep, sl_rep, 'firstname')
        self.middlename = self.__get_complete_value(
            gt_rep, sl_rep, 'middlename')
        self.lastname = self.__get_complete_value(
            gt_rep, sl_rep, 'lastname')
        self.name_suffix = self.__get_complete_value(
            gt_rep, sl_rep, 'name_suffix')
        self.nickname = self.__get_complete_value(
            gt_rep, sl_rep, 'nickname')
        self.gender = self.__get_complete_value(
            gt_rep, sl_rep, 'gender')
        self.birthday = self.__get_complete_value(
            gt_rep, sl_rep, 'birthday')
        self.chamber_id = self.__get_chamber_id(
            gt_rep, sl_rep)
        self.state_id = self.__get_state_id(
            gt_rep, sl_rep)
        self.rank_id = self.__get_rank_id(
            gt_rep, sl_rep)
        self.party = self.__get_complete_value(
            gt_rep, sl_rep, 'party')
        self.senate_class_id = self.__get_class_id(
            gt_rep, sl_rep)
        self.title_id = self.__get_title_id(
            gt_rep, sl_rep)
        self.leadership_id = self.__get_leadership_id(
            gt_rep)
        self.term_start = self.__get_complete_value(
            gt_rep, sl_rep, 'term_start')
        self.term_end = self.__get_complete_value(
            gt_rep, sl_rep, 'term_end')
        self.office_add = self.__get_complete_value(
            gt_rep, sl_rep, 'office_add')
        self.phone_no = self.__get_complete_value(
            gt_rep, sl_rep, 'phone_no')
        self.fax_no = self.__get_complete_value(
            gt_rep, sl_rep, 'fax_no')
        self.oc_email = self.__get_complete_value(
            gt_rep, sl_rep, 'oc_email')
        self.website = self.__get_complete_value(
            gt_rep, sl_rep, 'website')
        self.facebook_id = self.__get_complete_value(
            gt_rep, sl_rep, 'facebook_id')
        self.twitter_id = self.__get_complete_value(
            gt_rep, sl_rep, 'twitter_id')
        self.youtube_id = self.__get_complete_value(
            gt_rep, sl_rep, 'youtube_id')
        self.govtrack_id = self.__get_complete_value(
            gt_rep, sl_rep, 'govtrack_id')
        self.bioguide_id = self.__get_complete_value(
            gt_rep, sl_rep, 'bioguide_id')
        self.os_id = self.__get_complete_value(
            gt_rep, sl_rep, 'os_id')
        self.votesmart_id = self.__get_complete_value(
            gt_rep, sl_rep, 'votesmart_id')
        self.cspan_id = self.__get_complete_value(
            gt_rep, sl_rep, 'cspan_id')
        self.icpsr_id = self.__get_complete_value(
            gt_rep, sl_rep, 'icpsr_id')
        self.thomas_id = self.__get_complete_value(
            gt_rep, sl_rep, 'thomas_id')
        self.govtrack_link = self.__get_complete_value(
            gt_rep, sl_rep, 'govtrack_link')
        self.fec_ids = self.__get_complete_value(
            gt_rep, sl_rep, 'fec_ids')
        self.__log_representative()

    def __get_complete_value(self, gt_rep, sl_rep, key):
        """
        Check the values in the Govtrack and Sunlight Foundation responses and
        select a complete value. Some of the fields are blank in one or the
        other. If they are both blank None is returned.
        """
        if key in gt_rep and not key == "":
            return gt_rep[key]
        elif key in sl_rep and not key == "":
            return sl_rep[key]
        else:
            return None

    def __get_chamber_id(self, gt_rep, sl_rep):
        """
        Returns the correct chamber id from the database. The Chamber table
        is used as a foreign key in the Representative table. This method may
        be moved in the future.
        """
        value = self.__get_complete_value(gt_rep, sl_rep, 'chamber')
        if value == "Sen":
            value = "Senate"
        elif value == "Pre" or value == "Vic":
            value = "Executive"
        else:
            value = "House"

        logging.debug("Chamber during __get_chamber_id(): %s" % value)
        cur = self.database.cursor()
        cur.execute("SELECT id FROM Chamber \
            WHERE label = '%s'" % value)
        return cur.fetchone()[0]

    def __get_state_id(self, gt_rep, sl_rep):
        """
        Returns the correct state id from the database. The States table
        is used as a foreign key in the Representative and Role tables.
        """
        value = self.__get_complete_value(gt_rep, sl_rep, 'state')
        if value is None or value == '':
            return -1

        logging.debug("State during __get_state_id(): %s" % value)
        cur = self.database.cursor()
        cur.execute("SELECT id FROM States \
            WHERE short_name = '%s'" % value)
        return cur.fetchone()[0]

    def __get_rank_id(self, gt_rep, sl_rep):
        """
        Returns the correct rank id from the database. The Rank table
        is used as a foreign key in the Representative table.
        """
        value = self.__get_complete_value(
            {'rank': gt_rep['rank']},
            {'rank': sl_rep['state_rank'] if 'state_rank' in
             sl_rep.iterkeys() else ""},
            'rank')

        if value is None:
            return -1
        else:
            value = value.capitalize()

        logging.debug("Rank during __get_rank_id(): %s" % value)
        cur = self.database.cursor()
        cur.execute("SELECT id FROM Senate_Rank \
            WHERE label = '%s'" % value)
        return cur.fetchone()[0]

    def __get_class_id(self, gt_rep, sl_rep):
        """
        Returns the correct class id. Class ids are set to directly match
        the class number in the database, so there isn't a need to check the
        table for the correct id. -1 is a Null label because Class only
        exists in the Senate.
        """
        value = self.__get_complete_value(gt_rep, sl_rep, 'senate_class')
        logging.debug("Class during __get_class_id(): %s" % value)
        if value == '' or value is None:
            return -1
        else:
            return value

    def __get_title_id(self, gt_rep, sl_rep):
        """
        Returns the correct title id from the database. The Title table
        is used as a foreign key in the Representative table. Delegate is
        currently being used by any official from US territories, however,
        this needs to be split into two, Delegate and Commission.
        """
        value = self.__get_complete_value(gt_rep, sl_rep, 'title')
        if value == "Sen":
            value = "Senator"
        elif value == "Pre":
            value = "President"
        elif value == "Vic":
            value = "Vice President"
        elif value == "Rep" and not gt_rep['state'] == 'GU':
            value = "Representative"
        else:
            value = "Delegate"  # TODO: Seaparate into two titles

        logging.debug("Title during __get_title_id(): %s %s" %
                      (value, self.lastname))
        cur = self.database.cursor()
        cur.execute("SELECT id FROM Title \
            WHERE label = '%s'" % value)
        return cur.fetchone()[0]

    def __get_leadership_id(self, gt_rep):
        """
        Returns the correct leadership title id from the database. The title
        is returned from govtrack so only the govtrack results are provided.
        The tables is used as a foreign key in the Representative table.
        """
        if gt_rep["leadership_title"] is None:
            return -1

        value = gt_rep["leadership_title"]

        logging.debug("Leadership Title during __get_leadership_id(): %s %s" %
                      (value, self.lastname))
        cur = self.database.cursor()
        cur.execute("SELECT id FROM Leadership_Title \
            WHERE label = '%s'" % value)
        return cur.fetchone()[0]

    def get_representative(self):
        """
        Return a dictionary of the representative.
        """
        return {
            "firstname":        self.firstname,
            "middlename":       self.middlename,
            "lastname":         self.lastname,
            "name_suffix":      self.name_suffix,
            "nickname":         self.nickname,
            "gender":           self.gender,
            "birthday":         self.birthday,
            "chamber_id":       self.chamber_id,
            "state_id":         self.state_id,
            "rank_id":          self.rank_id,
            "party":            self.party,
            "senate_class_id":  self.senate_class_id,
            "title_id":         self.title_id,
            "leadership_id":    self.leadership_id,
            "term_start":       self.term_start,
            "term_end":         self.term_end,
            "office_add":       self.office_add,
            "phone_no":         self.phone_no,
            "fax_no":           self.fax_no,
            "oc_email":         self.oc_email,
            "website":          self.website,
            "facebook_id":      self.facebook_id,
            "twitter_id":       self.twitter_id,
            "youtube_id":       self.youtube_id,
            "govtrack_id":      self.govtrack_id,
            "bioguide_id":      self.bioguide_id,
            "os_id":            self.os_id,
            "votesmart_id":     self.votesmart_id,
            "cspan_id":         self.cspan_id,
            "icpsr_id":         self.icpsr_id,
            "thomas_id":        self.thomas_id,
            "govtrack_link":    self.govtrack_link,
            "fec_ids":          self.fec_ids
        }

    def write_to_database(self):
        """
        Write the representative to the representative table in the database.
        This will UPDATE a representative that exists and INSERT one that
        doesn't. Then call to write Federal Election Commission IDs to the
        database.
        """
        cur = self.database.cursor()

        cur.execute("SELECT id FROM representative          \
            WHERE govtrack_id = '%s';" % self.govtrack_id)
        if cur.fetchone():
            cur.execute("UPDATE representative SET          \
                firstname       = %s,                       \
                middlename      = %s,                       \
                lastname        = %s,                       \
                name_suffix     = %s,                       \
                nickname        = %s,                       \
                gender          = %s,                       \
                birthday        = %s,                       \
                chamber_id      = %s,                       \
                state_id        = %s,                       \
                rank_id         = %s,                       \
                party           = %s,                       \
                senate_class_id = %s,                       \
                title_id        = %s,                       \
                leadership_id   = %s,                       \
                term_start      = %s,                       \
                term_end        = %s,                       \
                office_add      = %s,                       \
                phone_no        = %s,                       \
                fax_no          = %s,                       \
                oc_email        = %s,                       \
                website         = %s,                       \
                facebook_id     = %s,                       \
                twitter_id      = %s,                       \
                youtube_id      = %s,                       \
                bioguide_id     = %s,                       \
                os_id           = %s,                       \
                votesmart_id    = %s,                       \
                cspan_id        = %s,                       \
                icpsr_id        = %s,                       \
                thomas_id       = %s,                       \
                govtrack_link   = %s                        \
                WHERE govtrack_id = '%s';", (
                self.firstname,
                self.middlename,
                self.lastname,
                self.name_suffix,
                self.nickname,
                self.gender,
                self.birthday,
                self.chamber_id,
                self.state_id,
                self.rank_id,
                self.party,
                self.senate_class_id,
                self.title_id,
                self.leadership_id,
                self.term_start,
                self.term_end,
                self.office_add,
                self.phone_no,
                self.fax_no,
                self.oc_email,
                self.website,
                self.facebook_id,
                self.twitter_id,
                self.youtube_id,
                self.bioguide_id,
                self.os_id,
                self.votesmart_id,
                self.cspan_id,
                self.icpsr_id,
                self.thomas_id,
                self.govtrack_link,
                self.govtrack_id))
        else:
            cur.execute("SELECT max(id) FROM representative;")
            self.repid = cur.fetchone()[0]
            if self.repid is None:
                self.repid = 1
            else:
                self.repid += 1
            cur.execute("INSERT INTO representative (       \
                id,                                         \
                firstname,                                  \
                middlename,                                 \
                lastname,                                   \
                name_suffix,                                \
                nickname,                                   \
                gender,                                     \
                birthday,                                   \
                chamber_id,                                 \
                state_id,                                   \
                rank_id,                                    \
                party,                                      \
                senate_class_id,                            \
                title_id,                                   \
                leadership_id,                              \
                term_start,                                 \
                term_end,                                   \
                phone_no,                                   \
                website,                                    \
                twitter_id,                                 \
                youtube_id,                                 \
                govtrack_id,                                \
                bioguide_id,                                \
                os_id,                                      \
                votesmart_id,                               \
                cspan_id,                                   \
                govtrack_link                               \
                ) VALUES (                                  \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s                                          \
                );",
                        (
                            self.repid,
                            self.firstname,
                            self.middlename,
                            self.lastname,
                            self.name_suffix,
                            self.nickname,
                            self.gender,
                            self.birthday,
                            self.chamber_id,
                            self.state_id,
                            self.rank_id,
                            self.party,
                            self.senate_class_id,
                            self.title_id,
                            self.leadership_id,
                            self.term_start,
                            self.term_end,
                            self.phone_no,
                            self.website,
                            self.twitter_id,
                            self.youtube_id,
                            self.govtrack_id,
                            self.bioguide_id,
                            self.os_id,
                            self.votesmart_id,
                            self.cspan_id,
                            self.govtrack_link
                        ))
        self.database.commit()
        cur.execute("SELECT id FROM representative          \
            WHERE govtrack_id = '%s';" % self.govtrack_id)
        self.repid = cur.fetchone()[0]
        if self.fec_ids is not None:
            self.__write_fec_ids(cur)
        cur.close()

    def __write_fec_ids(self, cur):
        """
        INSERT any new Federal Election Commission IDs associated with the
        representative into the fec_id table and link them.
        """
        for fec_id in self.fec_ids:
            cur.execute("SELECT fid FROM fec_id          \
                WHERE fid = '%s';" % fec_id)
            if cur.fetchone():
                continue
            else:
                cur.execute("select max(id) FROM fec_id")
                fid = cur.fetchone()[0]
                if fid is None:
                    fid = 1
                else:
                    fid += 1
                cur.execute("INSERT INTO fec_id (           \
                    id,                                     \
                    rep_id,                                 \
                    fid                                     \
                    ) VALUES (                              \
                    %s,                                     \
                    %s,                                     \
                    %s                                      \
                    );",
                            (
                                fid,
                                self.repid,
                                fec_id
                            ))
        self.database.commit()

    def __log_representative(self):
        """
        Print all data that is stored about the rep into the log under DEBUG.
        """
        logging.debug("Current Representative: \n\
            firstname = %s\n\
            middlename = %s\n\
            lastname = %s\n\
            name_suffix = %s \n\
            nickname = %s \n\
            gender = %s \n\
            birthday = %s \n\
            chamber_id = %s \n\
            state_id = %s \n\
            rank_id = %s \n\
            party = %s \n\
            senate_class_id = %s \n\
            title_id = %s \n\
            leadership_id = %s \n\
            term_start = %s \n\
            term_end = %s \n\
            office_add = %s \n\
            phone_no = %s \n\
            fax_no = %s \n\
            oc_email = %s \n\
            website = %s \n\
            facebook_id = %s \n\
            twitter_id = %s \n\
            youtube_id = %s \n\
            govtrack_id = %s \n\
            bioguide_id = %s \n\
            os_id = %s \n\
            votesmart_id = %s \n\
            cspan_id = %s \n\
            icpsr_id = %s \n\
            thomas_id = %s \n\
            govtrack_lin = %s \n\
            fec_ids = %s \n" % (
            self.firstname,
            self.middlename,
            self.lastname,
            self.name_suffix,
            self.nickname,
            self.gender,
            self.birthday,
            self.chamber_id,
            self.state_id,
            self.rank_id,
            self.party,
            self.senate_class_id,
            self.title_id,
            self.leadership_id,
            self.term_start,
            self.term_end,
            self.office_add,
            self.phone_no,
            self.fax_no,
            self.oc_email,
            self.website,
            self.facebook_id,
            self.twitter_id,
            self.youtube_id,
            self.govtrack_id,
            self.bioguide_id,
            self.os_id,
            self.votesmart_id,
            self.cspan_id,
            self.icpsr_id,
            self.thomas_id,
            self.govtrack_link,
            self.fec_ids))
