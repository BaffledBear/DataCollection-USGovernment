# DataCollection-USGovernment
This is a data collector that I was writing for a project I was working on with a number of other people. The group died out while I was working on this, so I am keeping it for posterity. 

I no longer have a copy of the config file, as it had sensetive info in it, and I have since had a harddrive failure. The purpose of this tool was to collect data from various APIs on members of the US goverment. The implementation is not what I would do in the future if I were to reattempt this, as the APIs that are currently being called are providing information that is available in yaml format [here on the @unitedstates github](https://github.com/unitedstates). It would be more effecient to parse the yaml files for the initial data and only call the APIs for information not present in those files. 

I am very much interested in the work subject and would be interested in work related to it. However, it's a big project and I don't know if I would want to take it on as a side project again, as it prevented me from accomplishing other goals. 

This tool specifically makes calls to APIs provided by the Sunlight Foundation and GovTrack.us. There was also research being done on integrating data from OpenSecrets, VoteSmart, FollowTheMoney, MapLight and Politifact as well as census data direct from the US government. 
